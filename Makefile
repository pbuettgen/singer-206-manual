# -*- coding: utf-8 -*-
#
# Copyright © 2020-2020 Philipp Büttgenbach
#
# This work is licensed under the Creative Commons
# Attribution-ShareAlike 4.0 International License.  To view a copy of
# this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
#

Images = $(wildcard images/*.png) $(wildcard images/*.svg)
Manual = singer-206-manual

%.html: %.adoc
	asciidoctor -b xhtml5 $<

%.epub: %.adoc ${Images}
	asciidoctor-epub3 $<

html: $(Manual).html

epub: $(Manual).epub

check: $(Manual).epub
	epubcheck $<

clean:
	rm -f $(Manual).epub

.PHONY: html epub check clean

# Local Variables:
# coding: utf-8
# mode: Makefile
# End:
